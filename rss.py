#!/usr/bin/env python3.4
# -*- encoding: utf-8 -*-
# -*- coding: utf-8 -*-

import feedparser
import sqlite3
import time
import urllib.request
import re

try:  
	#Create DB
	conn = sqlite3.connect('rss.db')
	c = conn.cursor()
except:
    input("No se ha podido esteblecer conexion con el servidor.")
    exit

#Create table
c.execute('''CREATE TABLE IF NOT EXISTS rss
           (id integer primary key AUTOINCREMENT not null,
           date text not null, 
           title text not null, 
           description text not null, 
           link text not null, 
           published text not null, 
           type real not null,
           state real default 1)''')


def download():
	
	#Insert Data RSS
	rss_url = "http://elcomercio.pe/feed/portada.xml"
	rss_url_2 = "http://www.rpp.com.pe/rss-loultimo.xml"
	feed = feedparser.parse(rss_url)

	date = time.strftime("%d/%m/%y")

	for item in feed['items']:
	    c.execute('''INSERT INTO rss(date,title,description,link,published,type) VALUES(?,?,?,?,?,?)''',(date,item['title'],item['description'],item['link'],item['published'],1))

	print("Datos descargados....")
	returnMenu()   

def viewNews():
	#View Data
	date = time.strftime("%d/%m/%y")
	c.execute('SELECT count(*) FROM rss where type = 1')
	result = c.fetchone()
	
	if result[0] != 0:
		c.execute('''SELECT * FROM rss where type = 1  ''')
		print('Noticias del Comercio - ', date + '\n')
		for item in c:
			print('Noticia ', item[0], ':', item[2])
			#print('Descripción: ', item[3] + '\n')
		print("Ok!")
	else:
		print('Primero debe seleccionar opción 1 ')
	
	returnMenu()

def download2():
	#Insert Data RSS
	rss_url = "http://www.rpp.com.pe/rss-loultimo.xml"
	feed = feedparser.parse(rss_url)

	date = time.strftime("%d/%m/%y")

	for item in feed['items']:
	    c.execute('''INSERT INTO rss(date,title,description,link,published,type) VALUES(?,?,?,?,?,?)''',(date,item['title'],item['description'],item['link'],item['published'],2))

	print("Datos descargados....")
	returnMenu()   

def viewNews2():
	#View Data
	date = time.strftime("%d/%m/%y")
	c.execute('''SELECT count(*) FROM rss where type = 2''')
	result = c.fetchone()
	if result[0] != 0:
		print('Noticias del RPP - ', date + '\n')
		c.execute('''SELECT * FROM rss where type = 2  ''')
		for item in c:
			print('Noticia ', item[0], ':', item[2])
			#print('Descripción: ', item[3] + '\n')
		print("Ok!")	
	else:
		print('Primero debe seleccionar opción 2 ')

	returnMenu()

def exchangeRate():
	info = urllib.request.urlopen ('http://www.sunat.gob.pe/cl-at-ittipcam/tcS01Alias')
	page = info.read()

	page = str(page)
	#print("My type is ", type(page))

	patron_page = r'class="H3">(.*)\\r\\n </table>\\r\\n</center>'
	patron_table = r'class="tne10">([0-9.]+)<\/td>'

	rest = re.findall(patron_page,page)
	rest = str(rest)

	rr = ["\\r","\\n","\\t","\\"," "]
	for i in rr:
		rest = rest.replace(i, "")

	rest = re.findall(patron_table,rest)
	print('Compra',rest[-2])
	print('Venta ',rest[-1])

	returnMenu()


def returnMenu():

    print("------------------------------------------------------------")
    input("\nPresione enter...")    
    menu()
        
def menu():
  
    print("""
    ----------------------------------------------------------------

             optiones disponibles

    ----------------------------------------------------------------

      1. - Descargar Noticias del Día - Comercio

      2. - Descargar Noticias del Día - RPP

      3. - Ver Noticias del Comercio

      4 -  Ver Noticias de RPP
      
      5.-  Ver Tipo de Cambio del Día $.
      
      6. - Salir.

   -----------------------------------------------------------------
   """)

    try:
        option = int(input("\n Ingrese una opción  "))
    except:    
        print("\nDato erroneo, por favor vuelva a intentarlo")
        returnMenu()

    try:
        if option == 1:
            download()
        elif option == 2:
            download2()
        elif option == 3:
            viewNews()
        elif option == 4:
            viewNews2()
        elif option == 5:
        	exchangeRate()
        elif option == 6:
            print("El programa se cerró...")
            conn.close()
        else:
            print("Error")
            returnMenu()
    except:
        print("\nError Final")
        returnMenu()

menu()
